<?php

/**
 * @file
 * Add an input filter for linking to other project nodes in casetracker.
 */

/**
 * Implementation of hook_theme().
 */
function projects_link_theme() {
  return array(
    'projects_link_issue_link' => array(
      'file' => 'project_link.module',
      'arguments' => array(
        'node' => NULL,
        'comment_id' => NULL,
        'comment_number' => NULL,
        'include_assigned' => FALSE,
      ),
    ),
  );
}

/**
 * Theme Project Issue links.
 *
 * @param $node
 *   The issue node object to be linked.
 * @param $comment_id
 *   The comment id to be appended to the link, optional.
 * @param $comment_number
 *   The comment's number, as visible to users, optional.
 * @param $include_assigned
 *   Optional boolean to include the user the issue is assigned to.
 */
function theme_projects_link_issue_link($node, $comment_id = NULL, $comment_number = NULL, $include_assigned = FALSE) {
  $path = "node/$node->nid";

  // See if the issue is assigned to anyone.  If so, we'll include it either
  // in the title attribute on hover, or next to the issue link if there was
  // an '@' appended to the issue nid.
  if (!empty($node->casetracker->assign_to)) {
    $account = user_load(array('uid' => $node->casetracker->assign_to));
    $username = theme('username', $account, array('plain' => TRUE));
  }
  else {
    $username = '';
  }

  $case_state = casetracker_case_state_load($node->casetracker->case_status_id, 'status');

  if (!empty($username) && !$include_assigned) {
    // We have an assigned user, but we're not going to print it next to the
    // issue link, so include it in title. l() runs $attributes through
    // drupal_attributes() which escapes the value.
    $attributes = array('title' => t('Status: !status, Assigned to: !username', array('!status' => $case_state, '!username' => $username)));
  }
  else {
    $attributes = array('title' => t('Status: !status', array('!status' => $case_state)));
  }


  if (isset($comment_id)) {
    $title = "#$node->nid-$comment_number: $node->title";
    $link = l($title, $path, array('attributes' => $attributes, 'fragment' => "comment-$comment_id"));
  }
  else {
    $title = "#$node->nid: $node->title";
    $link = l($title, $path, array('attributes' => $attributes));
  }

  $output = '<span class="project-issue-status-'. $node->casetracker->case_status_id .' project-issue-status-info">'. $link;
  if ($include_assigned && !empty($username)) {
    $output .= ' <span class="project-issue-assigned-user">'. t('Assigned to: @username', array('@username' => $username)) .'</span>';
  }
  $output .= '</span>';
  return $output;
}

/**
 * Implementation of hook_comment().
 */
function projects_link_comment(&$a1, $op) {
  if ($op == 'view') {
    drupal_add_css(drupal_get_path('module', 'projects_link') .'/projects_link.css', 'module');
  }
}

/**
 * Implementation of hook_form_filter_tips().
 */
function projects_link_filter_tips($delta, $format, $long = FALSE) {
  if ($long) {
    return t("References to Case Ids in the form of [#1234] (or [#1234-2] for comments) turn into links automatically, with the title of the issue appended. If '@' is appended (e.g. [#1234@]), the user that the case is assigned to will also be printed.");
  }
  else {
    return t('Case Id numbers (ex. [#12345]) turn into links automatically.');
  }
}

/**
 * Implementation of hook_filter().
 */
function projects_link_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(0 => t('Project Issue to link filter'));

    case 'description':
      return t('Converts references to project issues (in the form of [#12345]) into links. Caching should be disabled if node access control modules are used.');

    case 'no cache':
      return FALSE;

    case 'prepare':
      return $text;

    case 'process':
      $regex = '(?:(?<!\w)\[#\d+(?:-\d+)?(@)?\](?!\w))|<pre>.*?<\/pre>|<code>.*?<\/code>|<a(?:[^>"\']|"[^"]*"|\'[^\']*\')*>.*?<\/a>';
      $text = preg_replace_callback("/$regex/", 'projects_link_link_filter_callback', $text);
      return $text;
  }
}

function projects_link_link_filter_callback($matches) {
  $parts = array();
  if (preg_match('/^\[#(\d+)(?:-(\d+))?(@)?\]$/', $matches[0], $parts)) {
    $nid = $parts[1];
    $node = node_load($nid);
    $include_assigned = isset($parts[3]);
    if (is_object($node) && node_access('view', $node) && $node->type == 'casetracker_basic_case') {
      if (isset($parts[2])) {
        // Pull comment id based on the comment number if we have one.
        $comment_number = $parts[2];
        $comment_id = projects_link_get_comment_by_number($nid, $comment_number);
        if ($comment_id) {
          return theme('projects_link_issue_link', $node, $comment_id, $comment_number, $include_assigned);
        }
      }

      // If we got this far there wasn't a valid comment number, so just link
      // to the node instead.
      return theme('projects_link_issue_link', $node, NULL, NULL, $include_assigned);
    }
  }
  // If we haven't already returned a replacement, return the original text.
  return $matches[0];
}

/**
 * Implementation of hook_requirements().
 *
 * Check for conflicts with:
 *   installed node access control modules,
 *   'access project issues' restrictions,
 *   filters escaping code with higher weight.
 */
function projects_link_requirements($phase) {
  $requirements = array();
  $input_formats = array();
  if ($phase == 'runtime') {

    $grants = module_implements('node_grants');
    $allowed_roles = user_roles(FALSE, 'access project issues');
    $conflict_anonymous = empty($allowed_roles[DRUPAL_ANONYMOUS_RID]);

    foreach (filter_formats() as $format => $input_format) {
      $filters = filter_list_format($format);
      if (isset($filters['projects_link/0'])) {
        if (!empty($grants) && filter_format_allowcache($format)) {
          $requirements[] = array(
            'title' => t('Project Issue to link filter'),
            'value' => t('Some module conflicts were detected.'),
            'description' => t('%issuefilter should not be enabled when a node access control is also in use. Users may be able to see cached titles of project issues they would otherwise not have access to. You should disable this filter in !inputformat input format.', array('%issuefilter' => t('Project Issue to link filter'), '!inputformat' => l($input_format->name, "admin/settings/filters/$format"))),
            'severity' => REQUIREMENT_ERROR,
          );
        }

        if ($conflict_anonymous && filter_format_allowcache($format)) {
          $requirements[] = array(
            'title' => t('Project Issue to link filter'),
            'value' => t('Some security conflicts were detected.'),
            'description' => t('%issuefilter conflicts with project issue access settings. Users who do not have access to all project issues may be able to see titles of project issues. You should disable this filter in !inputformat input format.', array('%issuefilter' => t('Project Issue to link filter'), '!inputformat' => l($input_format->name, "admin/settings/filters/$format"))),
            'severity' => REQUIREMENT_ERROR,
          );
        }

        // Put up an error when some code escaping filter's weight is higher.
        $low_filters = array('filter/0', 'filter/1', 'bbcode/0', 'codefilter/0', 'geshifilter/0');
        foreach ($low_filters as $lfilter) {
          if (isset($filters[$lfilter]) && $filters['projects_link/0']->weight <= $filters[$lfilter]->weight) {
            $description_names['%issuefilter'] = $filters['projects_link/0']->name;
            $description_names['%lowfilter']  = $filters[$lfilter]->name;
            $requirements[] = array(
              'title' => t('Project Issue to link filter'),
              'value' => t('Some filter conflicts were detected.'),
              'description' => t('%issuefilter should come after %lowfilter to prevent loss of layout and highlighting.', $description_names) .' '. l(t('Please rearrange the filters.'), "admin/settings/filters/$format/order"),
              'severity' => REQUIREMENT_ERROR,
            );
          }
        }

      }
    }

  }
  return $requirements;
}

/**
 * Return an comment id based on node and order number.
 *
 * Loosely based on _ginkgo_get_comment_decay().
 */
function projects_link_get_comment_by_number($nid, $order) {
  static $timerange;
  if (!isset($timerange[$nid])) {
    $result = db_query("SELECT cid FROM {comments} WHERE nid = %d ORDER BY timestamp ASC", $nid);
    $i = 1;
    while ($row = db_fetch_object($result)) {
      $timerange[$nid][$i] = $row->cid;
      $i++;
    }
  }
  return $timerange[$nid][$order];
}
